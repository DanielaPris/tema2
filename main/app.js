function addTokens(input, tokens){
   

    if(typeof input!== typeof 'string'){
        throw new Error('Invalid input');
    }
  
   if(input.length<6){
    throw new Error('Input should have at least 6 characters');
   }

   for(let i=0;i<tokens.length;i++){
    if(typeof tokens[i].tokenName!=typeof 'string' || typeof tokens!= typeof [] ){
        throw new Error("Invalid array format");
     }
    
   }
   
    let words=input.split(' ');
    var modified=input;
    for(let i=0;i<words.length;i++){
        if(words[i]=="..."){
            for(let j=0;j<tokens.length;j++){
                modified=modified.replace(words[i],"${"+`${tokens[j].tokenName}`+"}");
            }
        }
   
    }

  return modified;
  
}


const app = {
    addTokens: addTokens
}

module.exports = app;
